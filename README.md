# aws-iam-packer

Terraform module to create and manage a user to build Packer templates.

## Usage

```hcl-terraform
module "iam_packer" {
  source = "git://gitlab.com/open-source-devex/terraform-modules/aws/iam-packer.git?ref=v1.0.1"
}
```
