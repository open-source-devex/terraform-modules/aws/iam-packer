#!/usr/bin/env sh

set -e

set -v

OVERRIDES_FILE=ci-overrides.tf

echo 'provider "aws" {
  region     = "string"
  access_key = "string"
  secret_key = "string"
}
' >  ${OVERRIDES_FILE}

terraform init
terraform validate .

rm -f ci-*.tf*
